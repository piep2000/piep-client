import {Injectable} from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';
import {HttpClient} from '@angular/common/http';
import {Platform} from '@ionic/angular';
import {from, Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    protected baseUrl = 'http://localhost:3000';

    public constructor(private platform: Platform, private http: HTTP, private httpClient: HttpClient) {
    }

    public live() {
        return this.request('get', '/live', {}, {});
    }

    public async messages(page: number, perPage: number = 3) {
        return await this.get(`/messages?page=${page}&perPage=${perPage}`, {}, {});
    }

    public async filters(page: number, perPage: number = 3) {
        return await this.get(`/messages?page=${page}&perPage=${perPage}`, {}, {});
    }

    protected async get(endPoint: string, params?: any, headers?: any) {
        return await this.requestPromise('get', endPoint, params, headers);
    }

    protected async post(endPoint: string, params?: any, headers?: any) {
        return await this.requestPromise('post', endPoint, params, headers);
    }

    protected async put(endPoint: string, params?: any, headers?: any) {
        return await this.requestPromise('put', endPoint, params, headers);
    }

    protected async delete(endPoint: string, params?: any, headers?: any) {
        return await this.requestPromise('delete', endPoint, params, headers);
    }

    protected request(type: string, endPoint: string, params?: any, headers?: any): Observable<any> {
        endPoint = (endPoint.startsWith('/') ? endPoint : `/${endPoint}`);

        if (this.platform.is('android') || this.platform.is('ios')) {
            return from(this.http[type](`${this.baseUrl}/${endPoint}`, params || {}, headers || {}));
        }
        return this.httpClient[type](`${this.baseUrl}${endPoint}`, {headers: headers || {}, params: params || {}});
    }

    protected async requestPromise(type: string, endPoint: string, params?: any, headers?: any) {
        return await this.request(type, endPoint, params, headers).toPromise();
    }
}
