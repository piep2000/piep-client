import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {
    public constructor(private storage: Storage) {
    }

    public async get<T>(key: string, defaultValue?: T): Promise<T> {
        const value = await this.storage.get(key);
        if (value === undefined) {
            return defaultValue;
        }
        return value;
    }

    public async set(key: string, value: any) {
        return await this.storage.set(key, value);
    }
}
