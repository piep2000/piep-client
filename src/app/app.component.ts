import {Component, Inject} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {DOCUMENT} from "@angular/common";
import {SettingsService} from "./settings.service";

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        @Inject(DOCUMENT) private document: Document,
        private settingsService: SettingsService
    ) {
        this.initializeApp();
    }

    async initializeApp() {
        await this.platform.ready();
        const val = await this.settingsService.get<boolean>('dark-mode', false);
        this.statusBar.styleDefault();
        if (val === true) {
            this.statusBar.backgroundColorByHexString('#222428');
            this.statusBar.styleLightContent();
            this.document.body.classList.add('dark');
        } else {
            this.statusBar.backgroundColorByHexString('#f4f5f8');
            this.statusBar.styleDefault();
        }
        this.splashScreen.hide();
    }
}
