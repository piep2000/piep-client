import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {AlertController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class StorageService {
    public constructor(private storage: Storage, private alertController: AlertController) {
    }

    public async size(): Promise<number> {
        let bytes = 0;
        const keys = await this.storage.keys();
        for (const key of keys) {
            bytes += this.getBytes(await this.storage.get(key));
        }
        return bytes;
    }

    public clear() {
        return new Promise(async (resolve, reject) => {
            const alert = await this.alertController.create({
                message: 'Are you sure you want to clear the cache?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            reject();
                        }
                    }, {
                        text: 'Yes',
                        handler: () => {
                            this.storage.clear().then(() => resolve());
                        }
                    }
                ]
            });
            await alert.present();
        });
    }

    public async length() {
        return await this.storage.length();
    }

    public async get<T>(key: string, defaultValue?: T): Promise<T> {
        const value = await this.storage.get(key);
        if (value === undefined) {
            return defaultValue;
        }
        return value;
    }

    public async set(key: string, value: any) {
        return await this.storage.set(key, value);
    }

    private bytes(s) {
        // noinspection TypeScriptUnresolvedFunction
        return ~-encodeURI(s).split(/%..|./).length;
    }

    private getBytes(s) {
        // noinspection TypeScriptUnresolvedFunction
        return this.bytes(JSON.stringify(s));
    }
}
