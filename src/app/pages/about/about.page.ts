import { Component } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import {Config, NavController} from '@ionic/angular';

@Component({
  selector: 'app-about',
  templateUrl: 'about.page.html',
  styleUrls: ['about.page.scss']
})
export class AboutPage {
  public constructor(private socialSharing: SocialSharing, private navCtrl: NavController, private config: Config) {

  }

  goBack() {
    this.navCtrl.pop();
  }

  public share() {
    this.socialSharing.share();
  }
}
