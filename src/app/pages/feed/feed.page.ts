import {Component} from '@angular/core';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {ApiService} from '../../api.service';
import {interval} from "rxjs";
import {startWith, switchMap} from "rxjs/operators";

@Component({
    selector: 'app-feed',
    templateUrl: 'feed.page.html',
    styleUrls: ['feed.page.scss']
})
export class FeedPage {
    public items: any[] = [];

    protected page = 1;

    public constructor(private apiService: ApiService, private socialSharing: SocialSharing) {
        this.getData();
    }

    ngOnInit() {
        interval(300 * 1000).pipe(startWith(0), switchMap(() => this.apiService.live())).subscribe(data => {
            if (data.length) {
                for (const item of data) {
                    if (!~this.items.indexOf(item)) {
                        item.date = new Date(item.date);
                        this.items.unshift(item);
                    }
                }
            }
        });
    }

    share(item) {
        this.socialSharing.share(item.name, item.year);
    }

    async getData() {
        const data = await this.apiService.live().toPromise();
        if (data.length) {
            for (const item of data) {
                if (!~this.items.indexOf(item)) {
                    item.date = new Date(item.date);
                    this.items.unshift(item);
                }
            }
        }
    }

    async loadData(event) {
        await this.getData();
        event.target.complete();

        // if scrolling downards, position to bottom and get old ones.
        // if scrolling upwards, position to top and only add new ones.

        // event.target.disabled = true;
    }
}
