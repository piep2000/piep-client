import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-language',
  templateUrl: 'language.page.html',
  styleUrls: ['language.page.scss']
})
export class LanguagePage {
  public constructor(private navCtrl: NavController) {

  }

  goBack() {
    this.navCtrl.pop();
  }
}
