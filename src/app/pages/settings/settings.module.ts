import {IonicModule} from '@ionic/angular';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SettingsPage} from './settings.page';
import {ComponentsModule} from '../../components/components.module';
import {SettingsPageRoutingModule} from './settings.router.module';


const routes: Routes = [
    {
        path: '',
        component: SettingsPage,
        children: [
            {
                path: 'language',
                children: [
                    {
                        path: '',
                        loadChildren: '../language/language.module#LanguagePageModule'
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ComponentsModule,
        SettingsPageRoutingModule
    ],
    declarations: [SettingsPage]
})
export class SettingsPageModule {
}
