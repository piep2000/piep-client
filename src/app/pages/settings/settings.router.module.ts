import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingsPage } from './settings.page';

const routes: Routes = [
    {
      path: '',
      component: SettingsPage,
      children: [
        {
          path: 'language',
          children: [
            {
              path: '',
              loadChildren: './language/language.module#LanguagePageModule'
            }
          ]
        }
      ]
    }
  ];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SettingsPageRoutingModule {}
