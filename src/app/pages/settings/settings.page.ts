import {Component, Inject} from '@angular/core';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {DOCUMENT} from "@angular/common";
import {SettingsService} from "../../settings.service";
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AlertController} from "@ionic/angular";
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StorageService} from "../../storage.service";

interface IItem {
    type: 'toggle' | 'router' | 'href' | 'header' | 'text';
    name: string;
    value?: any;
    callback?: any;
    route?: string;
    href?: string;
}

const readableBytes = (b: number, si: boolean = false): string => {
    const thresh = si ? 1000 : 1024;
    if (Math.abs(b) < thresh) {
        return b + ' B';
    }
    const units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    do {
        b /= thresh;
        ++u;
    } while (Math.abs(b) >= thresh && u < units.length - 1);
    return b.toFixed(1) + ' ' + units[u];
};

@Component({
    selector: 'app-settings',
    templateUrl: 'settings.page.html',
    styleUrls: ['settings.page.scss']
})
export class SettingsPage {
    protected items: IItem[];

    public showingItems: IItem[];

    public constructor(private socialSharing: SocialSharing,
                       @Inject(DOCUMENT) private document: Document,
                       private settingService: SettingsService,
                       private statusBar: StatusBar,
                       private storageService: StorageService,
                       private splashScreen: SplashScreen) {
    }

    async ionViewWillEnter() {
        this.items = [
            {
                type: 'header',
                name: 'Customize'
            },
            {
                type: 'toggle',
                value: await this.settingService.get<boolean>('dark-mode', false),
                name: 'Dark Mode',
                callback: this.toggleDarkMode.bind(this)
            },
            {
                type: 'router',
                route: '/home/settings/language',
                name: 'Language',
                value: 'English'
            },
            {
                type: 'router',
                route: '/home/settings/homescreen',
                name: 'Homescreen',
                value: 'Filters'
            },
            {
                type: 'header',
                name: 'Help'
            },
            {
                type: 'href',
                href: '/',
                name: 'Send Feedback'
            },
            {
                type: 'header',
                name: 'Cache'
            },
            {
                type: 'text',
                name: 'Local Cache',
                value: readableBytes(await this.storageService.size())
            },
            {
                type: 'text',
                name: 'Wipe Cache',
                callback: async () => {
                    this.storageService.clear().then(() => {
                        this.splashScreen.show();
                        location.reload();
                    });
                }
            },
            {
                type: 'header',
                name: 'Other'
            },
            {
                type: 'router',
                route: '/about',
                name: 'About'
            },
            {
                type: 'text',
                name: 'Version',
                value: 'v1.0.0'
            }
        ];
        this.showingItems = this.items;
    }

    public share() {
        this.socialSharing.share();
    }

    public async toggleDarkMode(item) {
        await this.settingService.set('dark-mode', item.value);
        if (item.value === true) {
            this.statusBar.backgroundColorByHexString('#222428');
            this.statusBar.styleLightContent();
            this.document.body.classList.add('dark');
        } else {
            this.statusBar.backgroundColorByHexString('#f4f5f8');
            this.statusBar.styleDefault();
            this.document.body.classList.remove('dark');
        }
    }

    public onChange($event) {
        const val = $event.detail.value.toLowerCase();
        if (val === '') {
            this.showingItems = this.items;
        } else {
            this.showingItems = this.items.filter(item => {
                return !!~item.name.toLowerCase().indexOf(val) ||
                    typeof item.value === 'string' && !!~item.value.toLowerCase().indexOf(val) ||
                    item.route && !!~item.route.toLowerCase().indexOf(val) ||
                    item.href && !!~item.href.toLowerCase().indexOf(val);
            });
        }
    }
}
