import { Component, ViewChild } from '@angular/core';
import { ApiService } from '../../api.service';
import { LargeTitleComponent } from '../../components/large-title/large-title.component';

@Component({
  selector: 'app-filters',
  templateUrl: 'filters.page.html',
  styleUrls: ['filters.page.scss']
})
export class FiltersPage {
  public customFilters: any[] = [];

  protected page = 1;

  @ViewChild(LargeTitleComponent) largeTitle: LargeTitleComponent;

  public constructor(private apiService: ApiService) {
    this.getFilters();
  }

  async getFilters() {
    const data = await this.apiService.filters(this.page);
    if (data.data.length) {
      for (const item of data.data) {
        if (!this.customFilters.filter(i => i.id === item.id).length) {
          this.customFilters.push(item);
        }
      }
      this.page = data.page + 1;
    }
  }

  async loadData(event) {
    await this.getFilters();
    event.target.complete();

    // if scrolling downards, position to bottom and get old ones.
    // if scrolling upwards, position to top and only add new ones.

    // event.target.disabled = true;
  }

  onScroll(event) {
    this.largeTitle.onScroll(event);
  }
}
