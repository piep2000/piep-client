import {Component, OnInit, Input, Output, ViewChild, Renderer2, EventEmitter} from '@angular/core';
import {IonHeader, Config, IonToolbar, IonSearchbar} from '@ionic/angular';

@Component({
    selector: 'app-large-title',
    templateUrl: './large-title.component.html',
    styleUrls: ['./large-title.component.scss'],
})
export class LargeTitleComponent implements OnInit {
    @Input() title: string;
    @Output()
    public ionChange: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(IonHeader) header: IonHeader;
    @ViewChild('smallTitle') smallTitle: IonToolbar;
    @ViewChild('largeTitle') largeTitle: IonToolbar;
    @ViewChild(IonSearchbar) searchBar: IonSearchbar;

    private platform: string;

    constructor(public renderer: Renderer2, config: Config) {
        this.platform = config.get('mode');
    }

    ngOnInit() {
        if (this.platform === 'ios') {
            const el = (<any>this.header).el;
            this.renderer.setStyle(el, 'transition', 'height 10ms, margin-top 300ms');
            this.renderer.setStyle((<any> this.largeTitle).el, 'transition', 'margin-bottom 200ms');
        }
    }

    public onChange($event) {
        this.ionChange.emit($event);
    }

    public onFocus($event) {
        this.searchBar.showCancelButton = true;
        this.renderer.setStyle((<any>this.largeTitle).el, 'margin-bottom', '30px');
        this.renderer.setStyle((<any>this.header).el, 'margin-top', '-118px');
    }

    public onBlur($event) {
        this.searchBar.showCancelButton = false;
        this.renderer.setStyle((<any>this.largeTitle).el, 'margin-bottom', '-6px');
        this.renderer.setStyle((<any>this.header).el, 'margin-top', '0px');
    }

    previousTop: number = 0;

    onScroll(event) {
        if (this.platform === 'ios') {
            const el = (<any>this.header).el;
            const height = el.clientHeight;
            const offsetTop = event.detail.scrollTop;
            const top = (offsetTop - this.previousTop);
            this.previousTop = offsetTop;

            // if (offsetTop < (142 - 88)) {
            //     this.renderer.setStyle(el, 'height', `${142 - offsetTop}px`);
            //     this.renderer.setStyle(event.target.children[0], 'margin-top', `-${offsetTop}px`);
            // }

            console.log(event);

            // if(offsetTop < 142) {
            //   if(height - top < height) {
            //     if(height - top >= 88) {
            //       el.style.height = `${height - top}px`;
            //     }
            //   } else if(height - top > height) {
            //     if(height - top <= 142) {
            //       el.style.height = `${height - top}px`;
            //     }
            //   }
            // }
        }
    }
}
