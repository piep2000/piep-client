import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LargeTitlePage } from './large-title.page';

describe('LargeTitlePage', () => {
  let component: LargeTitlePage;
  let fixture: ComponentFixture<LargeTitlePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LargeTitlePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LargeTitlePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
