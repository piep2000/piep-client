import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LargeTitleComponent } from './large-title/large-title.component';
import { IonicModule } from '@ionic/angular';
import {BackButtonComponent} from "./back-button/back-button.component";

@NgModule({
  declarations: [LargeTitleComponent, BackButtonComponent],
  imports: [
    IonicModule,
    CommonModule
  ],
  exports: [LargeTitleComponent, BackButtonComponent]
})
export class ComponentsModule { }
