import {Component, Input, OnInit} from '@angular/core';
import {Config} from '@ionic/angular';

@Component({
    selector: 'app-back-button',
    templateUrl: './back-button.component.html',
    styleUrls: ['./back-button.component.scss'],
})
export class BackButtonComponent implements OnInit {
    @Input()
    iosText: string;

    @Input()
    mdText: string;

    constructor(private config: Config) {
    }

    ngOnInit() {
    }
}
